#!/bin/sh

##
# Script for installing the components for timer on linux
##

# components
apt-get update
apt-get install git -y
apt-get install g++ -y
apt-get install nodejs -y
ln -s `which nodejs` /usr/bin/node
apt-get install npm -y
apt-get install libcurl4-openssl-dev -y
apt-get install libcurlpp-dev -y
apt-get install -t jessie vlc -y
apt-get install libvlccore-dev -y
apt-get install libvlc-dev -y
apt-get install pulseaudio -y
apt-get install mysql-client-core-5.6 -y
apt-get install mysql-client-5.6 -y
apt-get install libmysqlclient-dev -y
echo "mysql-server mysql-server/root_password password 123456" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password 123456" | debconf-set-selections
apt-get install mysql-server-5.6 -y
npm install -g bower
npm install -g forever

rm -rf tmp/
