#!/bin/sh
#
# install the autostart script on the system
#

cd timer/misc
cp timer_main /etc/init.d/
cp timer_website /etc/init.d/

chmod +x /etc/init.d/timer_main
chmod +x /etc/init.d/timer_website

update-rc.d timer_main defaults
update-rc.d timer_website defaults
