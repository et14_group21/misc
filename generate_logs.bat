@echo off
set log_directory="logs"
if not exist %log_directory% mkdir %log_directory%
for %%G in (*.*) DO (
	if not "%%G" == "generate_logs.bat" (
		echo Generating log for %%G
		git log --pretty=format:"%%H %%s (%%ad) <%%an>" --abbrev-commit %%G > %log_directory%/%%G.log
	)
)
echo.
echo All logs generated
pause