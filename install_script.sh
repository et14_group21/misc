#!/bin/sh

##
# Script for installing the timer on linux
##

CURRENT=`pwd`

# custom
rm -rf timer
mkdir timer
mkdir timer/config
cd timer
git config --global url."https://".insteadOf git://
git clone https://gitlab.com/et14_group21/main.git --recursive
git clone https://gitlab.com/et14_group21/database.git
git clone https://gitlab.com/et14_group21/website.git
git clone https://gitlab.com/et14_group21/misc.git

# mysql server
mysql -u root -p123456 < database/database_commands.sql

# website
cd website
npm install
cd public
bower install bower.json --force-latest
cd ..
cd ..

# main program
cd main
mv config/* ../config/
rmdir config/
make
cd ..

# autostart
cd misc/autostart/
sed -i "s|#PATH_TO_TIMER|$CURRENT|g" timer_main
sed -i "s|#PATH_TO_TIMER|$CURRENT|g" timer_website
