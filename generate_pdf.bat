@echo off
set word_path="C:\Program Files\Microsoft Office 15\root\office15\WINWORD.EXE"
for %%G in (*.docx) DO (
	echo Generating PDF %%G
	%word_path% /mExportToPDFext /q %%G
)
echo.
echo All pdfs generated
pause